import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    private baseUrl = "http://localhost:3000/users";

    constructor(private http: HttpClient){}

    GetUsers(){
        return this.http.get<any>(this.baseUrl);
    }

    GetUser(id: number){
        return this.http.get<any>(this.baseUrl + '/' + id);
    }


    async deleteUser(id:number){
        try {
            await this.http.delete<any>(this.baseUrl + '/' + id).toPromise();
            return true;
        } catch (error) {
            return false;
        }
        
    }

    async AddUser(newUser){
        return await this.http.post<any>(this.baseUrl, newUser).toPromise();
    }

    async EditUser(id: number, newUser: any){
        return await this.http.put<any>(this.baseUrl + '/' + id, newUser).toPromise();
    }
}