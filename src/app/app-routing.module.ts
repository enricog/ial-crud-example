import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestformComponent } from './components/testform/testform.component';
import { UsersComponent } from './components/users/users.component';
import { NewUserComponent } from './components/new-user/new-user.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';

const routes: Routes = [
  {
    path: 'hello',
    component: TestformComponent
  },
  {
    path: 'utenti',
    component: UsersComponent
  },
  {
    path: 'utenti/:id',
    component: EditUserComponent
  },
  {
    path: 'nuovoutente',
    component: NewUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
