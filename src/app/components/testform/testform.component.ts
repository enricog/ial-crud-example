import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-testform',
  template: `   
    <form #frm="ngForm" (submit)="send(frm)">
      <input type="text" ngModel name="firstname" placeholder="nome" required minlength="3"/><br/>
      <label class="error" 
        *ngIf="frm.controls.firstname?.invalid && frm.controls.firstname?.touched">
        inserire il nome
      </label><br/>
      <input type="text" ngModel name="lastname" placeholder="cognome" required/><br/>
      <label class="error" 
        *ngIf="frm.controls.lastname?.invalid && frm.controls.lastname?.touched">
        inserire il cognome
      </label><br/>
      <input type="email" ngModel name="email" placeholder="email" email/><br/>
      <label class="error" 
        *ngIf="frm.controls.email?.invalid && frm.controls.email?.touched">
        email errata
      </label><br/>
      <input type="text" ngModel name="phone" placeholder="telefono" required pattern="([+]|00)\d{1,}"/><br/>
      <label class="error" 
        *ngIf="frm.controls.phone?.invalid && frm.controls.phone?.touched">
        telefono errata
      </label><br/>
      <input type="submit" value="Invia"/>
      <input type="reset" value="Cancella"/>
      <ul>
        <li>valid: {{frm.valid}}</li>
        <li>touched: {{frm.touched}}</li>
        <li>dirty: {{frm.dirty}}</li>
        <li>pristine: {{frm.pristine}}</li>
      </ul>
    </form>

  `,
  styles: ['.error {color: red;}']
})
export class TestformComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  send(form: NgForm){
    console.log(form);
    if(form.valid) {
      console.log('invio al server ->',form.value);
      form.resetForm();
    }
    else{
      for(let c in form.controls) {
        form.controls[c].markAsTouched();
      }
    }
  }

}
