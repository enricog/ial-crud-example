import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-user',
  template: `
    <form #frm="ngForm" (submit)="add(frm)">
      <input type="text" name="firstname" placeholder="nome" ngModel required/><br>
      <input type="text" name="lastname" placeholder="cognome" ngModel required/><br>
      <input type="submit" value="Aggiungi"/>
    </form>
  `,
  styles: []
})
export class NewUserComponent implements OnInit {

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
  }

  async add(u: NgForm){
    if(u.valid){
      let res = await this.api.AddUser(u.value);
      this.router.navigate(['/utenti']);
    }
    else{
      alert('form invalida');
    }
  }

}
