import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  template: `
  <form  #frm="ngForm" (submit)="save(frm)">
    <input type="text" name="firstname" placeholder="nome" [ngModel]="user.firstname" required/><br>
    <input type="text" name="lastname" placeholder="cognome" [ngModel]="user.lastname"  required/><br>
    <input type="submit" [value]="submitLabel"/>
  </form>
  `,
  styles: []
})
export class EditUserComponent implements OnInit {

  user: any= {};
  isEdit: boolean;
  submitLabel: string;

  constructor(private api: ApiService, private router: Router, private active: ActivatedRoute) {    
    let id = this.active.snapshot.params.id;
    this.isEdit = id > 0;
    this.submitLabel = this.isEdit ? 'Modifica' : 'Aggiungi';
    if(this.isEdit){
      this.api.GetUser(id).subscribe(u => {
        this.user = u;
        //console.log(this.user);
      });
    }
  }

  ngOnInit() {
  }

  async save(u: NgForm){
    if(u.valid){
      if(this.isEdit){
        await this.api.EditUser(this.user.id, u.value);
      } else {
        await this.api.AddUser(u.value);
      }
      
      this.router.navigate(['/utenti']);
    }
    else{
      alert('form invalida');
    }
  }

}
