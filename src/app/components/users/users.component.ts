import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-users',
  template: `

  <a routerLink="/utenti/0">Aggiungi nuovo utente</a>
  <hr/>

    <ul>
      <li *ngFor="let pippo of users">
        <a routerLink="/utenti/{{pippo.id}}">{{pippo.firstname}} {{pippo.lastname}}</a>
        <a (click)="delete(pippo)">X</a>
      </li>
    </ul>
  `,
  styles: []
})
export class UsersComponent {
  users: any[];

  constructor(private api: ApiService) {
      this.api.GetUsers().subscribe(u => {
        this.users = u;
      });
   }

   async delete(user){
    if(confirm(`sicuro di voler cancellare ${user.firstname} ${user.lastname}?`)) {
      let res = await this.api.deleteUser(user.id);
      if(res){
        this.users = this.users.filter(x => x.id !== user.id);
      }
      console.log('risultato cancellazione ' + res);
     }
   }

}
